<?php include_once 'inc/config.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo SITENAME ?> : About Us</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <section class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/jito-logo-01.png" width="" height="38px" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="./">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services">SERVICE</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="about">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact">CONTACT</a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="container index">
        <div class="row justify-content-lg-center">
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-phone fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">We're on call 24/7</p>
                        <p style="margin-bottom: 0px">+2347084677847,</p>
                        <p>+2348092050055</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-clock-o fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Operating Hours</p>
                        <p>Mon - Fri 08:00 - 05:00</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-envelope fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Write To Us</p>
                        <p style="margin-bottom: 0px">info@jitomarinespl.com,</p>
                        <p style="margin-bottom: 0px">operations@jitomarinespl.com,</p>
                        <p>jitomarinespt@yahoo.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid back index1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>ABOUT US</h2>
                    <h4>HOME / ABOUT</h4>
                </div>
            </div>
        </div>
    </div>
    <section class="container about para" style="margin-top: -50px">
        <div class="row">
            <div class="col-md-8">
                <h2>COMPANY OVERVIEW</h2>
                <p>Jito Marine Support Limited (RC 933729) is an indigenous, Petroleum Products and Marine Services Company with a Local Multinational and Global perspective.</p>
                <p>Jito Marine Support Limited existed as Jito Consultancy Company in 2006 as an Enterprise being an offshoot of a merger of parent companies long established and highly respected, with histories dating back more than 10 years and then gained the status of a Limited Liability Company in 2010.</p>
                <p>It is a full Service Energy and marine trading Company, offering services as to capitalize on new opportunities in competitive energy market with an ongoing focus to enhance share holder value and meet customer’s needs by sustaining the financial strength, operational flexibility and skilled workforce needed to succeed in rapidly changing market conditions.</p>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <h2>OUR VISION</h2>
                        <p>To be a model of excellence in all the services we render with a focus to earn the confidence of all our numerous clients / customers.</p>
                    </div>
                    <div class="col-md-12">
                        <h2>OUR MISSION</h2>
                        <p>To create a strong foundation that is committed to setting the standard of excellence in everything we do. Our aim is to be the standard.</p>
                    </div>
                    <div class="col-md-12">
                        <h2>OUR GOALS</h2>
                        <p>Delivering prompt services Keeping to Industrial safety standard Treating our clients with all importance and dignity.</p>
                        <p>Doing the job right first time meeting our goals and targets ahead of schedule Constantly improving on the status quo.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid md">
        <div class="container">
            <div class="row">
                <div class="col-md-4" style="padding: 20px">
                    <img src="img/staff/CEO.png" height="200px" alt="">
                </div>
                <div class="col-md-8" style="padding: 20px">
                    <h3>Mr. Theophilus O Jacob</h3>
                    <h6>MD / CEO</h6>
                    <p>Mr. Thophilus O Jacob is a successful business man has vast experience on management of Petroleum haulage and supply with proven records to attest to his success. He initiated the establishment of Jito Consultancy Company which later metamorphoses to Jito Marine Support Limited and with God’s grace, he has taken the Company to her enviable height she attained today.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-md-4" style="text-align: center">
                <img src="img/staff/EC.png" height="150px" alt="" style="margin: 20px auto">
                <h3>Mrs. Juliet U. Jacob</h3>
                <h6>Executive Director</h6>
                <p>a seasoned professional businesses administer who together with Mr. Theophilus O. Jacob steered the ship of this Company to her enviable height.</p>
            </div>
            <div class="col-md-4" style="text-align: center">
                <img src="img/staff/BS.png" height="200px" alt="" style="margin: 20px auto">
                <h3>Mr Gabriel Olisakwe</h3>
                <h6>Business Development & General Manager</h6>
                <p>a professional Technical Engineer with several years of job experience in Marine. He holds a Bachelor in Science (B.Sc.) in Business Administration.</p>
            </div>
            <div class="col-md-4" style="text-align: center">
                <img src="img/staff/ED.png" height="150px" alt="" style="margin: 20px auto">
                <h3>Mr Olajide Wale</h3>
                <h6>Finance Director</h6>
                <p>handles our overall account section with 10 years job experience. He holds a Bachelor in Science (B.Sc.) in Accounting, HSE 1-3 and also has ICAN in view.</p>
            </div>
        </div>
    </section>
    <section class="container-fluid" style="background-color: lightgray">
        <div class="container">
            <div class="row">
                <div class="col-md-4" style="text-align: center">
                    <img src="img/staff/AC.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Mrs. Linda Ayanlechi</h3>
                    <h6>Warri Base Manager</h6>
                    <p>a graduate from Abraka University, Delta State and holds a Bachelor in Science (B.Sc.) in Accounting and finance and also certificate in Computer. She has vast experience in stock and shares bureau de.</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="img/staff/SC.png" height="200px" alt="" style="margin: 20px auto">
                    <h3>Mrs Vivian I. Zadiouse</h3>
                    <h6>Operation Secretary (P.H)</h6>
                    <p>a diploma in Computer Science and has worked with Albatross Marine Support Limited, Warri and TSK & Associates Lagos as the Company Secretary.</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="img/staff/WB.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Miss Miriam Okorie</h3>
                    <h6>Accountant</h6>
                    <p>a B.Sc and has been performing excellently in record keeping and accounting activities for the company.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-4" style="text-align: center">
                    <img src="img/staff/FC.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Mr Edward Ochelebe</h3>
                    <h6>Operations Manager (P.H)</h6>
                    <p>holds a B.Sc in Business Management with several year of experience on the job</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="img/staff/CEO.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Mr. Honour Edokpia</h3>
                    <h6>Administrative Manager (Warri)</h6>
                    <p>holds a Bachelor in Science (B.Sc.) in Business Administration and he has vast experience on various businesses.</p>
                </div>
                <div class="col-md-4" style="text-align: center">
                    <img src="img/staff/RP.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Miss Happiness Alerechi</h3>
                    <h6>Receptionist</h6>
                    <p>handles the reception activities of the company with her B.Sc in Marketing..</p>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid" style="background-color: lightgray">
        <div class="container">
            <div class="row">
                <div class="col-md-3" style="text-align: center">
                    <img src="img/staff/FC.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Mr. Ogundiran Zaccheaus A.</h3>
                    <h6>Administrative Manager (P.H)</h6>
                    <p>holds a HND in Banking and Finance and also HSE 1,2 and 3 and he has vast experience on various businesses </p>
                </div>
                <div class="col-md-3" style="text-align: center">
                    <img src="img/staff/CEO.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Mr. George Oju</h3>
                    <h6>Safety Officer</h6>
                    <p>holds a Bachelor in Science (B.Sc.) in Geology and also HSE 1,2, and 3.</p>
                </div>
                <div class="col-md-3" style="text-align: center">
                    <img src="img/staff/RP.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Miss Joy Omokiniovo</h3>
                    <h6>Secretary (Warri)</h6>
                    <p>holds HND in Business Adminstration, she also has 2 years working experience</p>
                </div>
                <div class="col-md-3" style="text-align: center">
                    <img src="img/staff/FC.png" height="150px" alt="" style="margin: 20px auto">
                    <h3>Mr. Andrew Ovuakporaye</h3>
                    <h6>Operations Manager (Warri)</h6>
                    <p>holds HND in Mechanical Engineering, He has various experience in marine operations, information and communication technology.</p>
                </div>
            </div>
        </div>
    </section>
    <footer class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>© 2020 JITO MARINE. All rights reserved | Design by <a href="http://www.intellitech.ng" target="NEW">INTELLITECH</a></p>
                </div>
            </div>
        </div>
    </footer>
    <script src='js/jquery-3.2.1.slim.min.js'></script>
    <script src="js/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>