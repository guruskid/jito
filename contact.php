<?php include_once 'inc/config.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo SITENAME ?> : Contact Us</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <section class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/jito-logo-01.png" width="" height="38px" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="./">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services">SERVICE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about">ABOUT</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="contact">CONTACT</a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="container index">
        <div class="row justify-content-lg-center">
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-phone fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">We're on call 24/7</p>
                        <p style="margin-bottom: 0px">+2347084677847,</p>
                        <p>+2348092050055</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-clock-o fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Operating Hours</p>
                        <p>Mon - Fri 08:00 - 05:00</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-envelope fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Write To Us</p>
                        <p style="margin-bottom: 0px">info@jitomarinespl.com,</p>
                        <p style="margin-bottom: 0px">operations@jitomarinespl.com,</p>
                        <p>jitomarinespt@yahoo.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid back index1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>CONTACT US</h2>
                    <h4>HOME / CONTACT</h4>
                </div>
            </div>
        </div>
    </div>

    <section class="container">
        <div class="row">
            <div class="col-md-4">
                <h5>HEAD OFFICE</h5>
                <i class="fa fa-home"></i>
                <p>108, Alcon Road Woji, Port Harcourt, Rivers State, Nigeria.</p>
                <i class="fa fa-phone"></i>
                <p class="text">+2348033895474, +2347084677847, +2348035363461</p>
                <i class="fa fa-envelope"></i>
                <p class="text">support@jitomarinespl.com</p>
            </div>
            <div class="col-md-4">
                <h5>BRANCH OFFICE</h5>
                <i class="fa fa-home"></i>
                <p>35, Airport Road Ugborikoko, Warri, Delta State, Nigeria.</p>
                <i class="fa fa-home"></i>
                <p class="text">+2348033895474, +2347084677847</p>
                <div class="clearfix"></div>
                <i class="fa fa-envelope"></i>
                <p class="text">support@jitomarinespl.com</p>
            </div>
            <div class="col-md-4">
                <h5>USA OFFICE</h5>
                <i class="fa fa-home"></i>
                <p>9959 Adleta BLVD, Dallas TX75243 Texas, United States.</p>
                <i class="fa fa-phone"></i>
                <p class="text">+16822084073</p>
                <div class="clearfix"></div>
                <i class="fa fa-envelope"></i>
                <p class="text">support@jitomarinespl.com</p>
            </div>
        </div>
        <div class="">
            <h4>Operation</h4>
            <ul>
                <li><i class="fa fa-phone"></i>
                <p class="text">+2348026424599, +2348051887202</p>
            </li>
            </ul>
        </div>
    </section>

    <section class="container" style="background-color: #444444; padding: 60px; margin-bottom: 10px">
        <div class="form-row">
            <div class="col-md-4" style="padding: 20px">
                <h5 style="color: #fff">DROP A MESSAGE HERE</h5><br><br>
                <p style="color: #fff; text-align: justify;">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
            </div>
            <div class="col-md-8">
                <form>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="name" placeholder="Your Name" class="form-control" style="border-radius: 0px">
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" name="email" placeholder="Your Email" class="form-control" style="border-radius: 0px">
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="text" name="email" placeholder="Your Subject" class="form-control" style="border-radius: 0px">
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea class="form-control" rows="7" style="border-radius: 0px"></textarea>
                        </div>
                        <div class="col-md-8 form-group" style="color: #fff">
                            <p>* Note: Your Email Will be kept Safe and Sound</p>
                        </div>
                        <div class="col-md-4 form-group" style="color: #fff">
                            <button type="submit" class="btn btn-danger" style="border-radius: 0px; margin-left: 60%">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <footer class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>© 2020 JITO MARINE. All rights reserved | Design by <a href="http://www.intellitech.ng" target="NEW">INTELLITECH</a></p>
                </div>
            </div>
        </div>
    </footer>
    <script src='js/jquery-3.2.1.slim.min.js'></script>
    <script src="js/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>