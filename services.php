<?php include_once 'inc/config.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo SITENAME ?> : Services</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<section class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/jito-logo-01.png" width="" height="38px" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="./">HOME</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="services">SERVICE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact">CONTACT</a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="container index">
        <div class="row justify-content-lg-center">
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-phone fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">We're on call 24/7</p>
                        <p style="margin-bottom: 0px">+2347084677847,</p>
                        <p>+2348092050055</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-clock-o fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Operating Hours</p>
                        <p>Mon - Fri 08:00 - 05:00</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-envelope fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Write To Us</p>
                        <p style="margin-bottom: 0px">info@jitomarinespl.com,</p>
                        <p style="margin-bottom: 0px">operations@jitomarinespl.com,</p>
                        <p>jitomarinespt@yahoo.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid back index1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>SERVICES</h2>
                    <h4>HOME / SERVICES</h4>
                </div>
            </div>
        </div>
    </div>
</body>
</html>