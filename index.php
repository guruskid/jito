<?php include_once 'inc/config.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo SITENAME ?></title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.css">

</head>

<body>
    <section class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">
                <img src="img/jito-logo-01.png" width="" height="38px" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="./">HOME</a>
                    </li>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        SERVICE
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">MARINE SERVICES &#038; LOGISTICS</a>
                        <a class="dropdown-item" href="#">WELL HEAD MAINTENANCE</a>
                        <a class="dropdown-item" href="#">CONSTRUCTION &#038; MAINTENANCE OF MARINE EQUIPMENT</a>
                        <a class="dropdown-item" href="#">VESSELS MANAGEMENT</a>
                        <a class="dropdown-item" href="#">CADET TRAINING/ VESSEL CREWING</a>
                        <a class="dropdown-item" href="#">PROVISION OF ESCORT FOR MARINE EQUIPMENT</a>
                        <a class="dropdown-item" href="#">PIPELINE MAINTENANCE</a>
                        <a class="dropdown-item" href="#">PETROLEUM SERVICES OFFSHORE &#038; ONSHORE SUPPLIES</a>
                        <a class="dropdown-item" href="#">LEASING OF TUGBOAT, HOUSEBOATS, BARGES &#038; CRANE</a>
                        <a class="dropdown-item" href="#">CONSULTANCY</a>
                      </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact">CONTACT</a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
    <div class="container index">
        <div class="row justify-content-lg-center">
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-phone fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">We're on call 24/7</p>
                        <p style="margin-bottom: 0px">+2347084677847,</p>
                        <p>+2348092050055</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-clock-o fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Operating Hours</p>
                        <p>Mon - Fri 08:00 - 05:00</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 none content">
                <div class="well none">
                    <i class="fa fa-envelope fa-4x fa-fw"></i>
                    <div class="text">
                        <p style="color: #a3a6ae; margin-bottom: 0px">Write To Us</p>
                        <p style="margin-bottom: 0px">info@jitomarinespl.com,</p>
                        <p style="margin-bottom: 0px">operations@jitomarinespl.com,</p>
                        <p>jitomarinespt@yahoo.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide index1" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="img/IMG-20181211-WA0051.jpg" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" height="393px" src="img/IMG-20180919-WA0072.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" height="393px" src="img/50.jpg" alt="Second slide">
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <section class="container para">
        <div class="row">
            <div class="col-md-8">
                <h1>To be the top choice in Marine support Services Company in West Africa</h1>
                <p>To deliver exemplary transshipment operations, procurement and charter Services – on time, on budget – according to global safety and quality standards.</p>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <h2>VISION</h2>
                        <p>To be a model of excellence in all the services we render with a focus to earn the confidence of all our numerous clients / customers.</p>
                    </div>
                    <div class="col-md-12">
                        <h2>MISSION</h2>
                        <p>To create a strong foundation that is committed to setting the standard of excellence in everything we do. Our aim is to be the standard.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container para para1">
        <div class="row justify-content-lg-center">
            <div class="col-md-4">
                <i class="fa fa-cog fa-4x fa-fw"></i>
                <h4>Excellent Machinery</h4>
            </div>
            <div class="col-md-4">
                <i class="fa fa-leaf fa-4x fa-fw"></i>
                <h4>Eco Friendly</h4>
            </div>
            <div class="col-md-4">
                <i class="fa fa-bell fa-4x fa-fw"></i>
                <h4>On-Time Delivery</h4>
            </div>
        </div>
    </section>
    <section class="container-fluid services">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-md-4">
                    <h4 style="text-align: center">ONSHORE & OFFSHORE SERVICE</h4>
                    <p style="text-align: justify">Oil & Gas Services:- Majoring in the supply of Petroleum Product of Automatic Gasoline Oil Diesel to Oil & Gas Company for both Onshore and Offshore, Providing front and back solutions to problematic processes in the Marine and petroleum sector.</p>
                </div>
                <div class="col-md-4">
                    <h4>MARINE / ENGINEERING SERVICES</h4>
                    <p>Design, Construction and Periodic Maintenance, Servicing of all Marine Vessel, Equipment and Machines, Procurement, Installation and Maintenance of Industrial Machines, Supply of Marine machine parts, Supply of Petroleum Products of Automobile Gasoline Oil (A.G.O.).</p>
                </div>
                <div class="col-md-4">
                    <h4>SAFETY MANAGEMENT SERVICES</h4>
                    <p>Health, Safety Environment Policy, Safety Inspection, Safety Auditing Programs, Supply of PPES / Fire Fighting Gears</p>
                </div>
            </div>
        </div>
    </section>
    <section class="container about">
        <div class="row">
            <div class="col-md-6">
                <img src="img/13.jpg" alt="">
            </div>
            <div class="col-md-6">
                <h1>Jitomarine Support Limited</h1>
                <p>Jito Marine Support Limited is an indigenous Nigerian Company equipped by seasoned and professional team with expertise in Vessel Charter Services, Procurement</p>
                <p>To withstand that issue, higher school custom writings and college teachers subscribe to services on the world wide web. What’s more, the info inside the custom essays may be used as reading materials for learning. You might have found out that you will have to be great at getting people under you to do what should be accomplished. In the same way, editing and formatting of the essay needs to be performed by the essay writing firm hence a comprehensive package needs to be delivered to the customer. Of course, they know for sure how to complete a paper successfully. If you consider the essay and it’s unbiased you will know just what may happen in that type of job. An essay which you buy is a way for you to be in a position to practice more tunes and music on your saxophone if you’re in a band. As a result, the customized essay writing firm ought to be in apposition to promise and deliver quality essays in keeping with the sum paid.</p>
                <p>At Jito Marine, we adhere strictly to global operational standards, including the Oil Companies International Marine Forum OCIMF guidelines in the performance of our services, as well as the relevant Local Content, Cabotage and operational laws of the countries we operate in. Our operations are with high reputation for excellent quality and timely delivery, with a trained team, who understand the importance of time management and safety during Ship to ship transfers.</p>
            </div>
        </div>
    </section>
    <div class="brands">
        <div class="container">
            <h3>Partner</h3>
            <div class="row">
                <div class="col">
                    <div class="brands_slider_container">
                        <div class="owl-carousel owl-theme brands_slider">
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/c_1.png" alt="One"></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/c_2.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/c_3.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/c_4.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/c_5.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/Ebenco.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/Midwestern.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/Tectonic.png" alt=""></div>
                            </div>
                            <div class="owl-item">
                                <div class="brands_item d-flex flex-column justify-content-center"><img src="img/client/TOMET.png" alt=""></div>
                            </div>
                        </div> <!-- Brands Slider Navigation -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>© 2020 JITO MARINE. All rights reserved | Design by <a href="http://www.intellitech.ng" target="NEW">INTELLITECH</a></p>
                </div>
            </div>
        </div>
    </footer>
    <script src='js/jquery-3.2.1.slim.min.js'></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {

            if ($('.brands_slider').length) {
                var brandsSlider = $('.brands_slider');

                brandsSlider.owlCarousel({
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    nav: false,
                    dots: false,
                    autoWidth: true,
                    items: 7,
                    margin: 42
                });
            }
        });
    </script>
</body>

</html>